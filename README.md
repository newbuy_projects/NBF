# Instalacja

NBF to aplikacja node.js. Do odpalenia jej wymaga jest pobranie i zainstalowanie node.js
(https://nodejs.org/dist/v8.11.4/node-v8.11.4-x64.msi)

Następnie należy odpalić skrypt INSTALL.bat pobierający node_modules.

# Uruchamianie

Należy odpalić skrypt RUN.bat

![alt text](doc/screenshots/console.png)

A następnie wejść pod http://localhost:4200/home

![alt text](doc/screenshots/homepage.png)

# Konfiguracja IDE

Do developowania front-endu polecam IDE WebStorm (https://www.jetbrains.com/webstorm/)

![alt text](doc/screenshots/project.png)

Konfiguracja uruchamiania:

![alt text](doc/screenshots/config.png)