export class Order {
  id: number;
  username: string;
  totalQuantity: number;
  productsCount: number;
  totalAmount: number;
  date: Date;
}
