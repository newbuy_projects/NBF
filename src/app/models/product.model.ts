import {Category} from './category.model';

export class Product {
  id: number;
  name: string;
  category = new Category();
  description: string;
  quantity: number;
  amount: number;
  availability: boolean;
  visibility: boolean;
}
