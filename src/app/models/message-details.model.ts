export class MessageDetails {
  mail: string;
  content: string;
  surname: string;
  firstname: string;
}
