export class UserDetails {
  username: string;
  password: string;
  mail: string;
  phone: string;
  surname: string;
  firstname: string;
}
