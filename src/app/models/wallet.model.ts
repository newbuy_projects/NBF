export class Wallet {
  id: number;
  balance: number;
  topUpDate: Date;
}
