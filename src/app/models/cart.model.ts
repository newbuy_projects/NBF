import {OrderItem} from './order-item.model';

export class Cart {
  items = new Array<OrderItem>();
  date: Date;
  username: string;
}
