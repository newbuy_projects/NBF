export class OrderItem {
  id: number;
  productId: number;
  quantity: number;
  name: string;
  amount: number;
}
