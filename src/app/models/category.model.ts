export class Category {
  id: number;
  name: string;
  promotion: number;
  visibility: boolean;
}
