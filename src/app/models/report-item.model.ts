export class ReportItem {
  productId: number;
  productName: string;
  date: Date;
  soldQuantity: number;
  purchasedAmount: number;
  soldTotalAmount: number;
}
