export class Comment {
  public id: number;
  public content: string;
  public username: string;

  constructor(id: number, content: string, username: string) {
    this.id = id;
    this.content = content;
    this.username = username;
  }
}
