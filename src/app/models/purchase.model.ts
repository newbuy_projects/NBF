export class Purchase {
  id: number;
  productName: string;
  productId: number;
  quantity: number;
  amount: number;
  date: Date;
}
