import {Injectable} from '@angular/core';
import {AuthStorage} from './auth-storage.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {User} from '../models/user.model';
import {TokenResponse} from '../models/token-response.model';

@Injectable()
export class AuthService {

  private userUrl = '/NBB';
  private plainTextResponse = {responseType: 'text' as 'text'};

  constructor(private http: HttpClient, private authStorage: AuthStorage, private router: Router) {
  }

  public attemptAuth(credentials) {
    const encodedCredentials = {'username': credentials.username, 'password': btoa(credentials.password)};
    return this.http.post<TokenResponse>(this.userUrl + '/token/generate-token', encodedCredentials);
  }

  public logout(withRedirect) {
    this.http.get(this.userUrl + '/token/invalidate', this.plainTextResponse).subscribe(
      response => {
        this.authStorage.signOut();
        if (withRedirect) {
          this.router.navigate(['/home']);
        }
      },
      err => console.log(err)
    );
  }

  public refresh() {
    this.http.get(this.userUrl + '/token/check', this.plainTextResponse).subscribe(
      response => {
        if (response === 'valid') {
          this.authStorage.setAuthenticated();
        } else {
          this.authStorage.signOut();
        }
      },
      err => {
        console.log(err);
        this.authStorage.signOut();
      }
    );
  }

  public updateUserData() {
    this.http.get<User>(this.userUrl + '/user/current').subscribe(
      response => {
        this.authStorage.saveUsername(response.username);
        this.authStorage.setAdmin(response.admin);
      },
      err => console.log(err)
    );
  }

  public isAuthenticated() {
    return this.authStorage.getIsAuthenticated();
  }

  public isAdmin() {
    return this.authStorage.getIsAdmin();
  }

  public getUsername() {
    return this.authStorage.getUsername();
  }

  saveToken(token) {
    this.authStorage.saveToken(token);
    this.updateUserData();
  }
}
