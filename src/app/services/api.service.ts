import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Product} from '../models/product.model';
import {Comment} from '../models/comment.model';
import {IdResponse} from '../models/id-response.model';
import {productRates} from '../models/product-rates.model';
import {CodeResponse} from '../models/code-response.model';
import {UserDetails} from '../models/user-details.model';
import {MessageDetails} from '../models/message-details.model';
import {Cart} from '../models/cart.model';
import {Wallet} from '../models/wallet.model';
import {Order} from '../models/order.model';
import {Category} from '../models/category.model';
import {Purchase} from '../models/purchase.model';
import {User} from '../models/user.model';
import {Photo} from '../models/photo.model';
import {ReportItem} from '../models/report-item.model';

@Injectable({
  providedIn: 'root'
})

export class APIService {
  API_URL = '/NBB';

  constructor(private httpClient: HttpClient) {
  }

  getProducts() {
    return this.httpClient.get<Product[]>(`${this.API_URL}/product`);
  }

  getAllProducts() {
    return this.httpClient.get<Product[]>(`${this.API_URL}/product/all`);
  }

  addProduct(product) {
    return this.httpClient.post(`${this.API_URL}/product`, product);
  }

  updateProduct(product) {
    return this.httpClient.put(`${this.API_URL}/product/` + product.id, product);
  }

  showProduct(productId) {
    return this.httpClient.get(`${this.API_URL}/product/show/` + productId);
  }

  hideProduct(productId) {
    return this.httpClient.get(`${this.API_URL}/product/hide/` + productId);
  }

  enableProduct(productId) {
    return this.httpClient.get(`${this.API_URL}/product/enable/` + productId);
  }

  disableProduct(productId) {
    return this.httpClient.get(`${this.API_URL}/product/disable/` + productId);
  }

  getProductsFromCategory(categoryId) {
    return this.httpClient.get(`${this.API_URL}/category/products/` + categoryId);
  }

  getCategories() {
    return this.httpClient.get<Category[]>(`${this.API_URL}/category`);
  }

  getAllCategories() {
    return this.httpClient.get<Category[]>(`${this.API_URL}/category/all`);
  }

  addCategory(category) {
    return this.httpClient.post(`${this.API_URL}/category`, category);
  }

  updateCategory(category) {
    return this.httpClient.put(`${this.API_URL}/category/` + category.id, category);
  }

  showCategory(categoryId) {
    return this.httpClient.get(`${this.API_URL}/category/show/` + categoryId);
  }

  hideCategory(categoryId) {
    return this.httpClient.get(`${this.API_URL}/category/hide/` + categoryId);
  }

  getNotifications() {
    return this.httpClient.get(`${this.API_URL}/notification`);
  }

  getProduct(productId) {
    return this.httpClient.get<Product>(`${this.API_URL}/product/` + productId);
  }

  getComments(productId) {
    return this.httpClient.get<Comment[]>(`${this.API_URL}/comment/fromProduct/` + productId);
  }

  addComment(productId, content) {
    return this.httpClient.post<IdResponse>(`${this.API_URL}/comment`, { 'productId' : productId, 'content' : content});
  }

  deleteComment(commentId) {
    return this.httpClient.delete(`${this.API_URL}/comment/` + commentId);
  }

  getRates(productId) {
    return this.httpClient.get<productRates>(`${this.API_URL}/rate/fromProduct/` + productId);
  }

  getPhotos(productId) {
    return this.httpClient.get<Photo[]>(`${this.API_URL}/photo/fromProduct/` + productId);
  }

  getFirstPhotos(categoryId) {
    return this.httpClient.get<Photo[]>(`${this.API_URL}/photo/firstPhoto/` + categoryId);
  }

  deletePhoto(photoId) {
    return this.httpClient.delete(`${this.API_URL}/photo/` + photoId);
  }

  addPhoto(photo) {
    return this.httpClient.post(`${this.API_URL}/photo`, photo);
  }

  addRate(productId, rate) {
    return this.httpClient.post(`${this.API_URL}/rate`, { 'productId' : productId, 'rate' : rate});
  }

  addToCart(productId, quantity) {
    return this.httpClient.post(`${this.API_URL}/cart`, { 'productId' : productId, 'quantity' : quantity});
  }

  registerUser(userDetails) {
    const encoded = {
      'username': userDetails.username,
      'password': btoa(userDetails.password),
      'mail': userDetails.mail,
      'phone': userDetails.phone,
      'surname': userDetails.surname,
      'firstname': userDetails.firstname};
    return this.httpClient.post<CodeResponse>(`${this.API_URL}/user/register`, encoded);
  }

  updateUser(userDetails) {
    const encoded = {
      'username': userDetails.username,
      'password': btoa(userDetails.password),
      'mail': userDetails.mail,
      'phone': userDetails.phone,
      'surname': userDetails.surname,
      'firstname': userDetails.firstname};
    return this.httpClient.post(`${this.API_URL}/user/update`, encoded);
  }

  getUserDetails() {
    return this.httpClient.get<UserDetails>(`${this.API_URL}/user/details`);
  }

  sendMessage(messageDetails: MessageDetails) {
    return this.httpClient.post(`${this.API_URL}/notification/message`, messageDetails);
  }

  getCart() {
    return this.httpClient.get<Cart>(`${this.API_URL}/cart`);
  }

  deleteFromCart(id) {
    return this.httpClient.delete(`${this.API_URL}/cart/` + id);
  }

  getWallet() {
    return this.httpClient.get<Wallet>(`${this.API_URL}/wallet`);
  }

  order() {
    return this.httpClient.get(`${this.API_URL}/cart/order`);
  }

  getAllOrders() {
    return this.httpClient.get<Order[]>(`${this.API_URL}/cart/ordered/all`);
  }

  getOrders() {
    return this.httpClient.get<Order[]>(`${this.API_URL}/cart/ordered`);
  }

  getOrderById(id) {
    return this.httpClient.get<Cart>(`${this.API_URL}/cart/` + id);
  }

  getPurchases() {
    return this.httpClient.get<Purchase[]>(`${this.API_URL}/purchase`);
  }

  addPurchase(purchase) {
    return this.httpClient.post(`${this.API_URL}/purchase`, purchase);
  }

  getAllUsers() {
    return this.httpClient.get<User[]>(`${this.API_URL}/user`);
  }

  topUp(topUp) {
    console.log(topUp);
    return this.httpClient.post(`${this.API_URL}/wallet/topUp`, topUp);
  }

  getReportItems(dateFrom, dateTo, productId) {
    const reportFilterDto = { 'dateRangeDto' : { 'dateFrom' : dateFrom, 'dateTo' : dateTo}, 'productId': productId};
    return this.httpClient.post<ReportItem[]>(`${this.API_URL}/report`, reportFilterDto);
  }
}
