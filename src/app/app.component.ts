import {Component, OnInit} from '@angular/core';
import {APIService} from './services/api.service';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {AuthService} from './core/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public categories;
  public user;

  constructor(
    private authService: AuthService,
    private router: Router,
    private apiService: APIService,
    private http: HttpClient
  ) {
  }

  public logout() {
    this.authService.logout(true);
  }

  ngOnInit() {
    this.getCategories();
    this.authService.refresh();
  }

  updateUser(data) {
    this.user = data;
  }

  public getCategories() {
    this.apiService.getCategories().subscribe(
      data => this.categories = data,
      err => console.error(err)
    );
  }

  goToCategory(category) {
    if (category) {
      this.router.navigate(['/category', {id: category.id, name: category.name}]);
    }
  }

  authenticated(): boolean {
    return this.authService.isAuthenticated();
  }

  admin(): boolean {
    return this.authService.isAuthenticated() && this.authService.isAdmin();
  }

  normalUser(): boolean {
    return this.authService.isAuthenticated() && !this.authService.isAdmin();
  }

  username(): string {
    const username = this.authService.getUsername();
    return username == null ? '' : username;
  }
}
