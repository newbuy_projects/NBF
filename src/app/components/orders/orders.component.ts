import {Component, OnInit} from '@angular/core';
import {APIService} from '../../services/api.service';
import {Order} from '../../models/order.model';

@Component({
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  public orders = new Array<Order>();

  constructor(
    private apiService: APIService) {
  }

  ngOnInit() {
    this.getOrders();
  }

  public getOrders() {
    this.apiService.getOrders().subscribe(
      data => this.orders = data,
      err => console.error(err)
    );
  }

  public itemsNotEmpty() {
    return this.orders.length !== 0;
  }
}
