import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../core/auth.service';
import {Router} from '@angular/router';
import {APIService} from '../../services/api.service';
import {FormControl, FormGroup} from '@angular/forms';
import {Purchase} from '../../models/purchase.model';
import {Product} from '../../models/product.model';

@Component({
  templateUrl: './add-purchase.component.html',
  styleUrls: ['./add-purchase.component.css']
})
export class AddPurchaseComponent implements OnInit {
  public purchase = new Purchase();
  public products = new Array<Product>();
  public error = false;
  public success = false;

  constructor(private authService: AuthService, private router: Router, private apiService: APIService) {
  }

  purchaseForm = new FormGroup({
    quantity: new FormControl(),
    amount: new FormControl(),
    product: new FormControl()
  });

  ngOnInit() {
    this.getAllProducts();
  }

  public getAllProducts() {
    this.apiService.getAllProducts().subscribe(
      data => this.products = data,
      err => console.error(err)
    );
  }

  addPurchase(): void {
    this.purchase.quantity = this.purchaseForm.controls['quantity'].value;
    this.purchase.amount = this.purchaseForm.controls['amount'].value;
    this.purchase.productId = this.purchaseForm.controls['product'].value;
    this.error = false;
    this.success = false;
    this.apiService.addPurchase(this.purchase).subscribe(data =>
        this.success = true,
      err => {
        console.log(err);
        this.error = true;
      });
  }

  compare(val1, val2) {
    return val1.id === val2.id;
  }
}
