import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../core/auth.service';
import {Router} from '@angular/router';
import {UserDetails} from '../../models/user-details.model';
import {APIService} from '../../services/api.service';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  public userDetails = new UserDetails();
  public success = false;
  public error = false;
  public usernameOccupied = false;

  constructor(private authService: AuthService, private router: Router, private apiService: APIService) {
  }

  settingsForm = new FormGroup({
    username: new FormControl({value: '', disabled: true}),
    password: new FormControl(),
    mail: new FormControl(),
    phone: new FormControl(),
    surname: new FormControl(),
    firstname: new FormControl()
  });

  ngOnInit() {
    this.getUserDetails();
  }

  public getUserDetails() {
    this.apiService.getUserDetails().subscribe(
      data => {
        this.userDetails = data;
        this.updateForm();
      },
      err => console.error(err)
    );
  }

  save(): void {
    this.userDetails.password = this.settingsForm.controls['password'].value;
    this.userDetails.mail = this.settingsForm.controls['mail'].value;
    this.userDetails.phone = this.settingsForm.controls['phone'].value;
    this.userDetails.surname = this.settingsForm.controls['surname'].value;
    this.userDetails.firstname = this.settingsForm.controls['firstname'].value;
    this.error = false;
    this.success = false;
    this.usernameOccupied = false;
    this.apiService.updateUser(this.userDetails).subscribe(data =>
        this.success = true,
      err => {
        console.log(err);
        this.error = true;
      });
  }

  submitDisabled() {
    return this.settingsForm.controls['password'].invalid ||
      this.settingsForm.controls['mail'].invalid ||
      this.settingsForm.controls['phone'].invalid ||
      this.settingsForm.controls['surname'].invalid ||
      this.settingsForm.controls['firstname'].invalid;
  }

  updateForm() {
    this.settingsForm.controls['username'].setValue(this.userDetails.username);
    this.settingsForm.controls['password'].setValue(this.userDetails.password);
    this.settingsForm.controls['mail'].setValue(this.userDetails.mail);
    this.settingsForm.controls['phone'].setValue(this.userDetails.phone);
    this.settingsForm.controls['surname'].setValue(this.userDetails.surname);
    this.settingsForm.controls['firstname'].setValue(this.userDetails.firstname);
  }
}
