import {Component, OnInit} from '@angular/core';
import {APIService} from '../../services/api.service';
import {Photo} from '../../models/photo.model';
import {ActivatedRoute} from '@angular/router';

@Component({
  templateUrl: './manage-photos.component.html',
  styleUrls: ['./manage-photos.component.css']
})
export class ManagePhotosComponent implements OnInit {

  public photos = new Array<Photo>();
  public productId: number;
  public content: string;
  public deleteSuccess = false;
  public addSuccess = false;
  public error = false;

  constructor(
    private apiService: APIService,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.productId = +params.id;
      this.getPhotos();
    });
  }

  refresh() {
    this.deleteSuccess = false;
    this.addSuccess = false;
    this.error = false;
  }

  public getPhotos() {
    this.apiService.getPhotos(this.productId).subscribe(
      data => this.photos = data,
      err => console.error(err)
    );
  }

  getContent(photo: Photo) {
    if (photo) {
      return 'data:image/jpeg;base64,' + photo.content;
    } else {
      return '';
    }
  }

  public itemsNotEmpty() {
    return this.photos.length !== 0;
  }

  deletePhoto(photoId) {
    this.refresh();
    this.apiService.deletePhoto(photoId).subscribe(
      data => {
        this.deleteSuccess = true;
        this.getPhotos();
      },
      err => {
        console.error(err);
        this.error = true;
      }
    );
  }

  addPhoto() {
    document.getElementById('closeAddPhotoModalButton').click();
    this.refresh();
    const photo = new Photo();
    photo.productId = this.productId;
    photo.content = this.content;
    this.apiService.addPhoto(photo).subscribe(
      data => {
        this.addSuccess = true;
        this.getPhotos();
      },
      err => {
        console.error(err);
        this.error = true;
      }
    );
  }

  onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.content = reader.result.split(',')[1];
      };
    }
  }
}
