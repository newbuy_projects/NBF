import {Component, OnInit} from '@angular/core';
import {APIService} from '../../services/api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Photo} from '../../models/photo.model';

@Component({
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  public products;
  public categoryId: number;
  public categoryName: string;
  public photos = new Array<Photo>();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private apiService: APIService) {
  }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.categoryId = +params.id;
      this.categoryName = params.name;
      this.getProducts();
    });
  }

  public getProducts() {
    this.apiService.getProductsFromCategory(this.categoryId).subscribe(
      data => {
        this.products = data;
        this.getFirstPhotos();
      },
    err => console.error(err)
    );
  }

  public getFirstPhotos() {
    this.apiService.getFirstPhotos(this.categoryId).subscribe(
      data => this.photos = data,
      err => console.error(err)
    );
  }

  public findPhoto(productId) {
    const photos = this.photos.filter(photo => photo.productId === productId);
    if (photos.length === 0) { return ''; }
    return 'data:image/jpeg;base64,' + photos[0].content;
  }

  public goToProduct(productId) {
    this.router.navigate(['/product', {id: productId}]);
  }
}
