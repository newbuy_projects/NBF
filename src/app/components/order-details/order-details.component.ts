import {Component, OnInit} from '@angular/core';
import {APIService} from '../../services/api.service';
import {ActivatedRoute} from '@angular/router';
import {Cart} from '../../models/cart.model';
import {AuthService} from '../../core/auth.service';

@Component({
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent  implements OnInit {

  public order = new Cart();
  public orderId: number;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private apiService: APIService) {
  }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.orderId = +params.id;
      this.getOrderById(this.orderId);
    });
  }

  public getOrderById(id) {
    this.apiService.getOrderById(id).subscribe(
      data => this.order = data,
      err => console.error(err)
    );
  }

  public totalAmount() {
    if (!this.order) { return 0; }
    if (!this.order.items) { return 0; }
    let sum = 0;
    this.order.items.forEach(function (item) {
      sum += item.amount * item.quantity;
    });
    return sum;
  }

  username(): string {
    const username = this.authService.getUsername();
    return username == null ? '' : username;
  }
}
