import {Component} from '@angular/core';
import {AuthService} from '../../core/auth.service';
import {Router} from '@angular/router';
import {UserDetails} from '../../models/user-details.model';
import {APIService} from '../../services/api.service';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';

@Component({
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  public userDetails = new UserDetails();
  public error = false;
  public usernameOccupied = false;

  constructor(private authService: AuthService, private router: Router, private apiService: APIService, private formBuilder: FormBuilder) {
  }

  registerForm = new FormGroup ({
    username: new FormControl(),
    password: new FormControl(),
    mail: new FormControl(),
    phone: new FormControl(),
    surname: new FormControl(),
    regulations: new FormControl(),
    firstname: new FormControl()
  });

  register(): void {
    this.userDetails.username = this.registerForm.controls['username'].value;
    this.userDetails.password = this.registerForm.controls['password'].value;
    this.userDetails.mail = this.registerForm.controls['mail'].value;
    this.userDetails.phone = this.registerForm.controls['phone'].value;
    this.userDetails.surname = this.registerForm.controls['surname'].value;
    this.userDetails.firstname = this.registerForm.controls['firstname'].value;
    this.error = false;
    this.usernameOccupied = false;
    this.apiService.registerUser(this.userDetails).subscribe(data => {
        if (data.code === 0) {
          this.login();
        } else if (data.code === 2) {
          this.usernameOccupied = true;
        } else {
          this.error = true;
        }
      },
      err => {
        console.log(err);
        this.error = true;
      });
  }

  login(): void {
    const credentials = {username: this.userDetails.username, password: this.userDetails.password};
    this.authService.attemptAuth(credentials).subscribe(data => {
        this.authService.saveToken(data.token);
        this.router.navigate(['/home']);
      },
      err => {
        console.log(err);
        this.error = true;
      }
    );
  }

  submitDisabled() {
    return this.registerForm.controls['username'].invalid ||
            this.registerForm.controls['password'].invalid ||
            this.registerForm.controls['mail'].invalid ||
            this.registerForm.controls['phone'].invalid ||
            this.registerForm.controls['surname'].invalid ||
            this.registerForm.controls['regulations'].invalid ||
            this.registerForm.controls['firstname'].invalid;
  }
}
