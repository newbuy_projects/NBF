import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../core/auth.service';
import {Router} from '@angular/router';
import {APIService} from '../../services/api.service';
import {FormControl, FormGroup} from '@angular/forms';
import {TopUp} from '../../models/top-up.model';
import {User} from '../../models/user.model';

@Component({
  templateUrl: './top-up.component.html',
  styleUrls: ['./top-up.component.css']
})
export class TopUpComponent implements OnInit {
  public topUp = new TopUp();
  public users = new Array<User>();
  public error = false;
  public success = false;

  constructor(private authService: AuthService, private router: Router, private apiService: APIService) {
  }

  topUpForm = new FormGroup({
    user: new FormControl(),
    amount: new FormControl()
  });

  ngOnInit() {
    this.getAllUsers();
  }

  public getAllUsers() {
    this.apiService.getAllUsers().subscribe(
      data => this.users = data,
      err => console.error(err)
    );
  }

  addTopUp(): void {
    this.error = false;
    this.success = false;
    this.topUp.userId = this.topUpForm.controls['user'].value;
    this.topUp.amount = this.topUpForm.controls['amount'].value;
    console.log(this.topUp);
    this.apiService.topUp(this.topUp).subscribe(data =>
        this.success = true,
      err => {
        console.log(err);
        this.error = true;
      });
  }

  compare(val1, val2) {
    return val1.id === val2.id;
  }
}
