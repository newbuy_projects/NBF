import {Component, OnInit} from '@angular/core';
import {APIService} from '../../services/api.service';
import {Order} from '../../models/order.model';

@Component({
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  public orders = new Array<Order>();

  constructor(
    private apiService: APIService) {
  }

  ngOnInit() {
    this.getAllOrders();
  }

  public getAllOrders() {
    this.apiService.getAllOrders().subscribe(
      data => this.orders = data,
      err => console.error(err)
    );
  }

  public itemsNotEmpty() {
    return this.orders.length !== 0;
  }

  public totalAmount() {
    if (!this.orders) { return 0; }
    let sum = 0;
    this.orders.forEach(function (order) {
      sum += order.totalAmount;
    });
    return sum;
  }

  public totalCount() {
    if (!this.orders) { return 0; }
    let sum = 0;
    this.orders.forEach(function (order) {
      sum += order.totalQuantity;
    });
    return sum;
  }
}
