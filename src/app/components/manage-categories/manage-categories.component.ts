import {Component, OnInit} from '@angular/core';
import {APIService} from '../../services/api.service';
import {FormControl, FormGroup} from '@angular/forms';
import {Category} from '../../models/category.model';

@Component({
  templateUrl: './manage-categories.component.html',
  styleUrls: ['./manage-categories.component.css']
})
export class ManageCategoriesComponent implements OnInit {

  public categories = new Array<Category>();
  public modifySuccess = false;
  public addSuccess = false;
  public error = false;
  public category = new Category();

  constructor(
    private apiService: APIService) {
  }

  addCategoryForm = new FormGroup ({
    name: new FormControl(),
    promotion: new FormControl()
  });

  updateCategoryForm = new FormGroup ({
    name: new FormControl(),
    promotion: new FormControl()
  });

  ngOnInit() {
    this.getAllCategories();
  }

  public getAllCategories() {
    this.apiService.getAllCategories().subscribe(
      data => this.categories = data,
      err => console.error(err)
    );
  }

  addCategory(): void {
    this.refresh();
    this.category.name = this.addCategoryForm.controls['name'].value;
    this.category.promotion = this.addCategoryForm.controls['promotion'].value;
    this.apiService.addCategory(this.category).subscribe(
      data => {
        this.addSuccess = true;
        this.getAllCategories();
        this.category = new Category();
      },
      err => console.error(err)
    );
    document.getElementById('closeAddCategoryModalButton').click();
  }

  update(): void {
    this.refresh();
    document.getElementById('closeUpdateCategoryModalButton').click();
    this.category.name = this.updateCategoryForm.controls['name'].value;
    this.category.promotion = this.updateCategoryForm.controls['promotion'].value;
    this.apiService.updateCategory(this.category).subscribe(
      data => {
        this.modifySuccess = true;
        this.getAllCategories();
        this.category = new Category();
      },
      err => console.error(err)
    );
  }

  refresh() {
    this.modifySuccess = false;
    this.addSuccess = false;
    this.error = false;
  }

  public itemsNotEmpty() {
    return this.categories.length !== 0;
  }

  updateCategory(id: number) {
    this.refresh();
    this.category = this.categories.filter(item => item.id === id)[0];
    this.category.id = id;
    this.updateCategoryForm.controls['name'].setValue(this.category.name);
    this.updateCategoryForm.controls['promotion'].setValue(this.category.promotion);
    document.getElementById('showUpdateCategoryModalButton').click();
  }

  showCategory(id: number) {
    this.refresh();
    this.apiService.showCategory(id).subscribe(
      data => {
        this.modifySuccess = true;
        this.getAllCategories();
      },
      err => console.error(err)
    );
  }

  hideCategory(id: number) {
    this.refresh();
    this.apiService.hideCategory(id).subscribe(
      data => {
        this.modifySuccess = true;
        this.getAllCategories();
      },
      err => console.error(err)
    );
  }
}
