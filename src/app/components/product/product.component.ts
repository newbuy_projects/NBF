import {Component, OnInit} from '@angular/core';
import {APIService} from '../../services/api.service';
import {ActivatedRoute} from '@angular/router';
import {Product} from '../../models/product.model';
import {Comment} from '../../models/comment.model';
import {AuthService} from '../../core/auth.service';
import {Photo} from '../../models/photo.model';

@Component({
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  public product: Product;
  public productId: number;
  public commentId: number;
  public selectedQuantity = 1;
  public showInvalidQuantityMessage = false;
  public showInvalidCommentMessage = false;
  public showAddToCartSuccessMessage = false;
  public showAddToCartErrorMessage = false;
  public showAddCommentSuccessMessage = false;
  public showAddCommentErrorMessage = false;
  public showDeleteCommentSuccessMessage = false;
  public showDeleteCommentErrorMessage = false;
  public comments = new Array<Comment>();
  public photos = new Array<Photo>();
  public rateQuantity = 0;
  public averageRate: number;
  public rate = 0;
  public commentContent: string;

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private apiService: APIService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.productId = +params.id;
      this.getProducts();
      this.getComments();
      this.getRates();
      this.getPhotos();
    });
  }

  public getProducts() {
    this.apiService.getProduct(this.productId).subscribe(
      data => this.product = data,
      err => console.error(err)
    );
  }

  public getComments() {
    this.apiService.getComments(this.productId).subscribe(
      data => this.comments = data,
      err => console.error(err)
    );
  }

  public getRates() {
    this.apiService.getRates(this.productId).subscribe(
      data => {
        this.rateQuantity = data.quantity;
        this.averageRate = data.averageRate;
      },
      err => console.error(err)
    );
  }

  public getPhotos() {
    this.apiService.getPhotos(this.productId).subscribe(
      data => this.photos = data,
      err => console.error(err)
    );
  }

  public refreshFeedback() {
    this.showInvalidQuantityMessage = false;
    this.showInvalidCommentMessage = false;
    this.showAddToCartSuccessMessage = false;
    this.showAddToCartErrorMessage = false;
    this.showAddCommentSuccessMessage = false;
    this.showAddCommentErrorMessage = false;
    this.showDeleteCommentSuccessMessage = false;
    this.showDeleteCommentErrorMessage = false;
  }

  public nullSafeProductName() {
    return this.product ? this.product.name : '';
  }

  public nullSafeProductDescription() {
    return this.product ? this.product.description : '';
  }

  public nullSafeProductQuantity() {
    return this.product ? this.product.quantity : 0;
  }

  public nullSafeProductAmount() {
    return this.product ? this.product.amount : 0;
  }

  public nullSafeProductAvailability() {
    return this.product ? this.product.availability : false;
  }

  public totalAmount() {
    return this.selectedQuantity * this.nullSafeProductAmount();
  }

  public addToCart() {
    this.refreshFeedback();
    if (this.selectedQuantity < 1 || this.selectedQuantity > this.nullSafeProductQuantity()) {
      this.showInvalidQuantityMessage = true;
    } else {
      document.getElementById('closeAddToCartModalButton').click();
      this.apiService.addToCart(this.productId, this.selectedQuantity).subscribe(
        data => {
          this.showAddToCartSuccessMessage = true;
          this.product.quantity = this.product.quantity - this.selectedQuantity;
          this.selectedQuantity = 1;
          if (this.product.quantity < 1) {
            this.product.availability = false;
          }
        },
        err => {
          console.log(err);
          this.showAddToCartErrorMessage = true;
        });
    }
  }

  public addComment() {
    this.refreshFeedback();
    if (!this.commentContent) {
      this.showInvalidCommentMessage = true;
    } else {
      document.getElementById('closeAddCommentModalButton').click();
      this.apiService.addComment(this.productId, this.commentContent).subscribe(
        data => {
          this.showAddCommentSuccessMessage = true;
          const comment = new Comment(data.id, this.commentContent, this.getUsername());
          this.comments.push(comment);
          this.commentContent = '';
        },
        err => {
          console.log(err);
          this.showAddCommentErrorMessage = true;
        });
    }
  }

  public addRate() {
    this.refreshFeedback();
    this.apiService.addRate(this.productId, this.rate).subscribe(
      data => {
        this.getRates();
      },
      err => {
        console.log(err);
      });
  }

  public getUsername() {
    return this.authService.getUsername();
  }

  public showConfirmDeleteCommentModal(commentId) {
    this.commentId = commentId;
    document.getElementById('showDeleteCommentModalButton').click();
  }

  public deleteComment() {
    this.refreshFeedback();
    document.getElementById('closeDeleteCommentModalButton').click();
    this.apiService.deleteComment(this.commentId).subscribe(
      data => {
        this.showDeleteCommentSuccessMessage = true;
        this.comments = this.comments.filter(obj => obj.id !== this.commentId);
      },
      err => {
        console.log(err);
        this.showDeleteCommentErrorMessage = true;
      });
  }

  public getRateStarClass(i: number) {
    if (this.rate !== 0) {
      if (this.rate >= i) {
        return 'fa-star text-warning';
      } else {
        return 'fa-star-o text-warning';
      }
    } else {
      return i <= this.averageRate ? 'fa-star text-dark' : 'fa-star-o text-dark';
    }
  }

  authenticated(): boolean {
    return this.authService.isAuthenticated();
  }

  admin(): boolean {
    return this.authService.isAuthenticated() && this.authService.isAdmin();
  }

  normalUser(): boolean {
    return this.authService.isAuthenticated() && !this.authService.isAdmin();
  }

  getContent(photo: Photo) {
    if (photo) {
      return 'data:image/jpeg;base64,' + photo.content;
    } else {
      return '';
    }
  }

  additionalPhotos() {
    return this.photos.filter(photo => photo.id !== this.photos[0].id);
  }
}
