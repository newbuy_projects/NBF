import {Component, OnInit} from '@angular/core';
import {APIService} from '../../services/api.service';
import {Purchase} from '../../models/purchase.model';

@Component({
  templateUrl: './purchases.component.html',
  styleUrls: ['./purchases.component.css']
})
export class PurchasesComponent implements OnInit {

  public purchases = new Array<Purchase>();

  constructor(
    private apiService: APIService) {
  }

  ngOnInit() {
    this.getPurchases();
  }

  public getPurchases() {
    this.apiService.getPurchases().subscribe(
      data => this.purchases = data,
      err => console.error(err)
    );
  }

  public itemsNotEmpty() {
    return this.purchases.length !== 0;
  }
}
