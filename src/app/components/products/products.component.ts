import {Component, OnInit} from '@angular/core';
import {APIService} from '../../services/api.service';
import {Router} from '@angular/router';
import {Product} from '../../models/product.model';
import {Photo} from '../../models/photo.model';

@Component({
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  public products = new Array<Product>();
  public photos = new Array<Photo>();

  constructor(
    private apiService: APIService,
    private router: Router) {
  }

  ngOnInit() {
    this.getProducts();
  }

  public getProducts() {
    this.apiService.getProducts().subscribe(
      data => {
        this.products = data;
        this.getFirstPhotos();
      },
      err => console.error(err)
    );
  }

  public getFirstPhotos() {
    this.apiService.getFirstPhotos(-1).subscribe(
      data => this.photos = data,
      err => console.error(err)
    );
  }

  public findPhoto(productId) {
    const photos = this.photos.filter(photo => photo.productId === productId);
    if (photos.length === 0) { return ''; }
    return 'data:image/jpeg;base64,' + photos[0].content;
  }

  public goToProduct(productId) {
    this.router.navigate(['/product', {id: productId}]);
  }
}
