import {Component} from '@angular/core';
import {AuthService} from '../../core/auth.service';
import {Router} from '@angular/router';
import {APIService} from '../../services/api.service';
import {FormControl, FormGroup} from '@angular/forms';
import {MessageDetails} from '../../models/message-details.model';

@Component({
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {
  public messageDetails = new MessageDetails();
  public error = false;
  public success = false;

  constructor(private authService: AuthService, private router: Router, private apiService: APIService) {
  }

  contactForm = new FormGroup({
    mail: new FormControl(),
    content: new FormControl(),
    surname: new FormControl(),
    regulations: new FormControl(),
    firstname: new FormControl()
  });

  send(): void {
    this.messageDetails.mail = this.contactForm.controls['mail'].value;
    this.messageDetails.content = this.contactForm.controls['content'].value;
    this.messageDetails.surname = this.contactForm.controls['surname'].value;
    this.messageDetails.firstname = this.contactForm.controls['firstname'].value;
    this.error = false;
    this.success = false;
    this.apiService.sendMessage(this.messageDetails).subscribe(data =>
        this.success = true,
      err => {
        console.log(err);
        this.error = true;
      });
  }

  submitDisabled() {
    return this.contactForm.controls['mail'].invalid ||
      this.contactForm.controls['content'].invalid ||
      this.contactForm.controls['surname'].invalid ||
      this.contactForm.controls['regulations'].invalid ||
      this.contactForm.controls['firstname'].invalid;
  }
}
