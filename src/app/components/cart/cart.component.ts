import {Component, OnInit} from '@angular/core';
import {APIService} from '../../services/api.service';
import {Cart} from '../../models/cart.model';
import {Wallet} from '../../models/wallet.model';
import {Router} from '@angular/router';

@Component({
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  public cart = new Cart();
  public wallet = new Wallet();
  public orderSuccess = false;
  public deleteSuccess = false;
  public error = false;

  constructor(
    private apiService: APIService,
    private router: Router) {
  }

  ngOnInit() {
    this.getCart();
    this.getWallet();
  }

  public refreshFeedback() {
    this.orderSuccess = false;
    this.deleteSuccess = false;
    this.error = false;
  }

  public getCart() {
    this.apiService.getCart().subscribe(
      data => this.cart = data,
      err => console.error(err)
    );
  }

  public getWallet() {
    this.apiService.getWallet().subscribe(
      data => this.wallet = data,
      err => console.error(err)
    );
  }

  public itemsNotEmpty() {
    return this.cart.items.length !== 0;
  }

  public totalAmount() {
    if (!this.cart) { return 0; }
    if (!this.cart.items) { return 0; }
    let sum = 0;
    this.cart.items.forEach(function (item) {
      sum += item.amount * item.quantity;
    });
    return sum;
  }

  deleteFromCart(id: number) {
    this.refreshFeedback();
    this.apiService.deleteFromCart(id).subscribe(
      data => {
        this.getCart();
        this.deleteSuccess = true;
      }, err => {
        console.error(err);
        this.deleteSuccess = true;
      }
    );
  }

  order() {
    this.refreshFeedback();
    document.getElementById('closeOrderModalButton').click();
    this.apiService.order().subscribe(
      data => {
        this.orderSuccess = true;
        this.router.navigate(['/history']);
      },
      err => {
        console.error(err);
        this.error = true;
      }
    );
  }
}
