import {Component, OnInit} from '@angular/core';
import {APIService} from '../../services/api.service';
import {Product} from '../../models/product.model';
import {FormControl, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  templateUrl: './manage-products.component.html',
  styleUrls: ['./manage-products.component.css']
})
export class ManageProductsComponent implements OnInit {

  public categories;
  public products = new Array<Product>();
  public modifySuccess = false;
  public addSuccess = false;
  public error = false;
  public product = new Product();

  constructor(
    private router: Router,
    private apiService: APIService) {
  }

  addProductForm = new FormGroup ({
    name: new FormControl(),
    category: new FormControl(),
    description: new FormControl(),
    amount: new FormControl(),
  });

  updateProductForm = new FormGroup ({
    name: new FormControl(),
    category: new FormControl(),
    description: new FormControl(),
    amount: new FormControl(),
  });

  ngOnInit() {
    this.getAllProducts();
    this.getAllCategories();
  }

  public getAllCategories() {
    this.apiService.getAllCategories().subscribe(
      data => this.categories = data,
      err => console.error(err)
    );
  }

  addProduct(): void {
    this.refresh();
    this.product.name = this.addProductForm.controls['name'].value;
    this.product.category.id = this.addProductForm.controls['category'].value;
    this.product.description = this.addProductForm.controls['description'].value;
    this.product.amount = this.addProductForm.controls['amount'].value;
    this.apiService.addProduct(this.product).subscribe(
      data => {
        this.addSuccess = true;
        this.getAllProducts();
      },
      err => console.error(err)
    );
    document.getElementById('closeAddProductModalButton').click();
  }

  update(): void {
    this.refresh();
    document.getElementById('closeUpdateProductModalButton').click();
    this.product.name = this.updateProductForm.controls['name'].value;
    this.product.category.id = this.updateProductForm.controls['category'].value;
    this.product.description = this.updateProductForm.controls['description'].value;
    this.product.amount = this.updateProductForm.controls['amount'].value;
    this.apiService.updateProduct(this.product).subscribe(
      data => {
        this.modifySuccess = true;
        this.getAllProducts();
        this.product = new Product();
      },
      err => console.error(err)
    );
  }

  refresh() {
    this.modifySuccess = false;
    this.addSuccess = false;
    this.error = false;
  }

  public getAllProducts() {
    this.apiService.getAllProducts().subscribe(
      data => this.products = data,
      err => console.error(err)
    );
  }

  public itemsNotEmpty() {
    return this.products.length !== 0;
  }

  showProduct(id: number) {
    this.refresh();
    this.apiService.showProduct(id).subscribe(
      data => {
        this.modifySuccess = true;
        this.getAllProducts();
        this.product = new Product();
      },
      err => console.error(err)
    );
  }

  hideProduct(id: number) {
    this.refresh();
    this.apiService.hideProduct(id).subscribe(
      data => {
        this.modifySuccess = true;
        this.getAllProducts();
      },
      err => console.error(err)
    );
  }

  enableProduct(id: number) {
    this.refresh();
    this.apiService.enableProduct(id).subscribe(
      data => {
        this.modifySuccess = true;
        this.getAllProducts();
        this.product = new Product();
      },
      err => console.error(err)
    );
  }

  disableProduct(id: number) {
    this.refresh();
    this.apiService.disableProduct(id).subscribe(
      data => {
        this.modifySuccess = true;
        this.getAllProducts();
      },
      err => console.error(err)
    );
  }

  updateProduct(id: number) {
    this.refresh();
    this.product = this.products.filter(item => item.id === id)[0];
    this.product.id = id;
    this.updateProductForm.controls['name'].setValue(this.product.name);
    this.updateProductForm.controls['category'].setValue(this.product.category.id);
    this.updateProductForm.controls['description'].setValue(this.product.description);
    this.updateProductForm.controls['amount'].setValue(this.product.amount);
    document.getElementById('showUpdateProductModalButton').click();
  }

  compare(val1, val2) {
    return val1.id === val2.id;
  }

  goToPhotos(productId) {
    this.router.navigate(['/admin/photos', {id: productId}]);
  }
}
