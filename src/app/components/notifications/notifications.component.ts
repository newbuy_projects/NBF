import {Component, OnInit} from '@angular/core';
import {APIService} from '../../services/api.service';

@Component({
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

  public notifications;

  constructor(private apiService: APIService) {
  }

  ngOnInit() {
    this.getNotifications();
  }

  public getNotifications() {
    this.apiService.getNotifications().subscribe(
      data => this.notifications = data,
      err => console.error(err)
    );
  }
}
