import {Component} from '@angular/core';
import {AuthService} from '../../core/auth.service';
import {Router} from '@angular/router';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  public credentials = {username: '', password: ''};
  public error = false;

  constructor(private authService: AuthService, private router: Router) {
  }

  loginForm = new FormGroup ({
    username: new FormControl(),
    password: new FormControl()
  });

  login(): void {
    this.credentials.username = this.loginForm.controls['username'].value;
    this.credentials.password = this.loginForm.controls['password'].value;
    this.error = false;
    this.authService.attemptAuth(this.credentials).subscribe(data => {
        this.authService.saveToken(data.token);
        this.router.navigate(['/home']);
      },
      err => {
        console.log(err);
        this.error = true;
      }
    );
  }
}
