import {Component, OnInit} from '@angular/core';
import {APIService} from '../../services/api.service';
import {ReportItem} from '../../models/report-item.model';
import {FormControl, FormGroup} from '@angular/forms';
import {Product} from '../../models/product.model';

@Component({
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  public reportItems = new Array<ReportItem>();
  public products = new Array<Product>();
  public from: Date;
  public to: Date;
  public productId: number;

  constructor(
    private apiService: APIService) {
  }

  reportForm = new FormGroup ({
    from: new FormControl(),
    to: new FormControl(),
    product: new FormControl()
  });

  ngOnInit() {
    this.getReportItems();
    this.getAllProducts();
  }

  public getAllProducts() {
    this.apiService.getAllProducts().subscribe(
      data => this.products = data,
      err => console.error(err)
    );
  }

  refresh(): void {
    this.from = this.reportForm.controls['from'].value;
    this.to = this.reportForm.controls['to'].value;
    this.productId = this.reportForm.controls['product'].value;
    this.getReportItems();
  }

  public getReportItems() {
    this.apiService.getReportItems(this.from, this.to, this.productId).subscribe(
      data => this.reportItems = data,
      err => console.error(err)
    );
  }

  public itemsNotEmpty() {
    return this.reportItems.length !== 0;
  }

  public totalSoldQuantity() {
    if (!this.reportItems) { return 0; }
    let sum = 0;
    this.reportItems.forEach(function (reportItem) {
      sum += reportItem.soldQuantity;
    });
    return sum;
  }

  public totalSoldAmount() {
    if (!this.reportItems) { return 0; }
    let sum = 0;
    this.reportItems.forEach(function (reportItem) {
      sum += reportItem.soldTotalAmount;
    });
    return sum;
  }

  public totalPurchasedAmount() {
    if (!this.reportItems) { return 0; }
    let sum = 0;
    this.reportItems.forEach(function (reportItem) {
      sum += reportItem.purchasedAmount * reportItem.soldQuantity;
    });
    return sum;
  }

  public totalProfit() {
    if (!this.reportItems) { return 0; }
    let sum = 0;
    this.reportItems.forEach(function (reportItem) {
      sum += reportItem.soldTotalAmount - reportItem.purchasedAmount * reportItem.soldQuantity;
    });
    return sum;
  }

  compare(val1, val2) {
    return val1.id === val2.id;
  }
}
