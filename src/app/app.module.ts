import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {ProductsComponent} from './components/products/products.component';
import {ProductComponent} from './components/product/product.component';
import {HomeComponent} from './components/home/home.component';
import {PageNotFoundComponent} from './components/not-found/not-found.component';
import {CategoryComponent} from './components/category/category.component';
import {LoginComponent} from './components/login/login.component';
import {NotificationsComponent} from './components/notifications/notifications.component';
import {RegulationsComponent} from './components/regulations/regulations.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthService} from './core/auth.service';
import {Interceptor} from './core/inteceptor';
import {AuthStorage} from './core/auth-storage.service';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {RegisterComponent} from './components/register/register.component';
import {SettingsComponent} from './components/settings/settings.component';
import {ContactComponent} from './components/contact/contact.component';
import {CartComponent} from './components/cart/cart.component';
import {OrdersComponent} from './components/orders/orders.component';
import {OrderDetailsComponent} from './components/order-details/order-details.component';
import {ManageProductsComponent} from './components/manage-products/manage-products.component';
import {ManageCategoriesComponent} from './components/manage-categories/manage-categories.component';
import {ReportComponent} from './components/report/report.component';
import {PurchasesComponent} from './components/purchases/purchases.component';
import {AddPurchaseComponent} from './components/add-purchase/add-purchase.component';
import {TopUpComponent} from './components/top-up/top-up.component';
import {ManagePhotosComponent} from './components/manage-photos/manage-photos.component';
import {HistoryComponent} from './components/history/history.component';


const appRoutes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'products', component: ProductsComponent},
  {path: 'product', component: ProductComponent},
  {path: 'category', component: CategoryComponent},
  {path: 'cart', component: CartComponent},
  {path: 'history', component: OrdersComponent},
  {path: 'order', component: OrderDetailsComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'settings', component: SettingsComponent},
  {path: 'regulations', component: RegulationsComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'admin/notifications', component: NotificationsComponent},
  {path: 'admin/products', component: ManageProductsComponent},
  {path: 'admin/categories', component: ManageCategoriesComponent},
  {path: 'admin/report', component: ReportComponent},
  {path: 'admin/purchases', component: PurchasesComponent},
  {path: 'admin/purchase', component: AddPurchaseComponent},
  {path: 'admin/history', component: HistoryComponent},
  {path: 'admin/topUp', component: TopUpComponent},
  {path: 'admin/photos', component: ManagePhotosComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProductsComponent,
    ProductComponent,
    CategoryComponent,
    CartComponent,
    OrdersComponent,
    OrderDetailsComponent,
    LoginComponent,
    RegisterComponent,
    SettingsComponent,
    RegulationsComponent,
    ContactComponent,
    NotificationsComponent,
    ManageProductsComponent,
    ManageCategoriesComponent,
    ReportComponent,
    PurchasesComponent,
    AddPurchaseComponent,
    HistoryComponent,
    TopUpComponent,
    ManagePhotosComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFontAwesomeModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: false}  // true for debugging
    ),
    HttpClientModule
  ],
  providers: [AuthService, AuthStorage, { provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
